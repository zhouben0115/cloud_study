package com.dream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Date:2021/7/19 19:04
 * @Author: zhouben
 */

@SpringBootApplication
@EnableDiscoveryClient
public class ProviderApplication8001 {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication8001.class,args);
    }
}
