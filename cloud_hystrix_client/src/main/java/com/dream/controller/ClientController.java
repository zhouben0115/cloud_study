package com.dream.controller;

import com.dream.service.HystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Date:2021/6/30 20:14
 * @Author: zhouben
 */

@RestController
public class ClientController {


    @Autowired
    private HystrixService service;

    @HystrixCommand(fallbackMethod = "errorHandler",commandProperties ={
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "2000")
    })
    @GetMapping("/hystrix/getOK")
    public String getOk(){
        return service.paymentInfo_OK(10);
    }
//
    @HystrixCommand(fallbackMethod = "errorHandler",commandProperties ={
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "2000")
    })
    @GetMapping("/hystrix/getPort")
    public String getMessage(){
        return service.paymentInfo_TimeOut(10);
    }



    public String errorHandler(){

        return "error" ;
    }

}
