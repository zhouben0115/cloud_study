package com.dream.service.impl;

import com.dream.service.HystrixService;
import org.springframework.stereotype.Component;

/**
 * @Date:2021/7/4 11:05
 * @Author: zhouben
 */
@Component
public class HystrixServiceImpl  implements HystrixService {
    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "paymentInfo_TimeOut";
    }

    @Override
    public String paymentInfo_OK(Integer id) {
        return "paymentInfo_OK";
    }
}
