package com.dream.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * @Date:2021/7/11 18:12
 * @Author: zhouben
 */

@RestController
@RefreshScope
public class PayController {


    @Value("${server.port}")
    private String port;

   // @Value("${info}")
    String info;

    //    @RequestMapping(value = "/getPort",method = RequestMethod.GET)
    @GetMapping("/getPort")
    public String getPort() {


        LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd",Locale.CHINA));
        return "pay======>" + port;
    }

    @GetMapping("/getInfo")
    public String getInfo(){
        return info;
    }

}
