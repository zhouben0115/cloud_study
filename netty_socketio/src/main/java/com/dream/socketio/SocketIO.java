package com.dream.socketio;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIONamespace;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.SpringAnnotationScanner;
import com.corundumstudio.socketio.protocol.Packet;
import com.corundumstudio.socketio.protocol.PacketType;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @Date:2021/7/25 16:17
 * @Author: zhouben
 */
public class SocketIO {

    private static final List<SocketIOClient> clients = new ArrayList<>();

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.setPort(9999);
        configuration.setHostname("127.0.0.1");
        //configuration.setContext("/user");
        SocketIOServer server = new SocketIOServer(configuration);
        //SocketIONamespace socketIONamespace = server.addNamespace("/user");
        //System.out.println(socketIONamespace.getName());
        //System.out.println(socketIONamespace.getAllClients());
        server.addConnectListener(client->{
           // client.joinRoom("s");
            //System.out.println(client.getAllRooms());
            clients.add(client);
            System.out.println( "添加的远程++++++> " + client.getRemoteAddress());
        });


        server.addDisconnectListener(clients::remove);

        server.start();

        Timer timer = new Timer();

        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                Random random = new Random();
//                Packet packet = new Packet(PacketType.MESSAGE);
//                packet.setSubType(PacketType.EVENT);
//                packet.setName("message");
//                packet.setData(new Point(random.nextInt(100), random.nextInt(100)));

                for (SocketIOClient client : clients) {
                    Packet packet = new Packet(PacketType.MESSAGE);
                    packet.setSubType(PacketType.EVENT);
                    packet.setName("message");
                    packet.setData(Arrays.asList("java","python"));
                    //client.sendEvent("message", new Point(random.nextInt(100), random.nextInt(100)));

                    client.send(packet);
                }
                System.out.println("客户端的连接数===> ::::" + clients.size());
            }
        }, 1000, 1000);

    }
}
