package com.dream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Date:2021/7/25 16:07
 * @Author: zhouben
 */
@SpringBootApplication
public class SocketIOApplication {
    public static void main(String[] args) {
        SpringApplication.run(SocketIOApplication.class,args);
    }
}
