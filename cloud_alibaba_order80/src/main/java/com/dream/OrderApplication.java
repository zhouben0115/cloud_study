package com.dream;

import feign.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @Date:2021/7/11 18:24
 * @Author: zhouben
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

   // @Bean
    public Logger.Level level(){
        /**
         * NONE: 不显示
         * BASIC:仅记录请求方法、URL、响应状态码、执行时间
         * HEADERS: 除BASIC，还有请求和响应头的信息
         * FULL: 除HEADERS,还有请求和响应的正文及元数据
         */
        return Logger.Level.FULL;
//        return Logger.Level.HEADERS;
//        return Logger.Level.BASIC;
    }
}
