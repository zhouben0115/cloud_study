package com.dream.controller;

import com.dream.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @Date:2021/7/11 18:23
 * @Author: zhouben
 */


@RestController
public class OrderController {


    @Resource
    private RestTemplate template;

    private final String uri = "http://nacos-provider";


    @Autowired
    private OrderService orderService;
    @GetMapping("/getMessage")
    public String getMessage(){
       // return  template.getForObject(uri+"/getPort",String.class);
        return orderService.getPort();
    }
}
