package com.rule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Date:2021/6/27 17:40
 * @Author: zhouben
 */

@Configuration
public class LoadBalancerRule {

    @Bean
    public IRule rule(){
        return new RandomRule();
    }
}
