package com.dream.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Date:2021/6/27 18:17
 * @Author: zhouben
 */
@Component
public class LoadBalancerImpl implements LoadBalancer {

    private final AtomicInteger atomicInteger = new AtomicInteger(0);

    private int getIndex() {
        int current, next;
        for (; ; ) {
            current = this.atomicInteger.get();
            next = current >= Integer.MAX_VALUE ? 0 : current + 1;
            if (atomicInteger.compareAndSet(current, next))
                return next;
        }
//        do {
//            current = this.atomicInteger.get();
//            next = current >= Integer.MAX_VALUE ? 0 : current + 1;
//        }while(!this.atomicInteger.compareAndSet(current,next));
//        System.out.println("*****第几次访问，次数next: "+next);
//        return next;
    }

    @Override
    public ServiceInstance getServiceInstance(List<ServiceInstance> serviceInstances) {
        if(CollectionUtils.isEmpty(serviceInstances))
            return null;
        /**
         * 轮询算法
         *  请求数 % 总数 = 目标
         */
        //得到下表
        int count = getIndex();
        int index = count % serviceInstances.size();
        return serviceInstances.get(index);
    }
}
