package com.dream.controller;

import com.dream.entity.CommonResult;
import com.dream.entity.Payment;
import com.dream.lb.LoadBalancer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

/**
 * @Date:2021/6/22 20:16
 * @Author: zhouben
 */
@RestController
public class OrderController {
    //调用的地址
    private static final String url = "http://CLOUD-PROVIDER";

    @Autowired
    private LoadBalancer loadBalancer;

    @Autowired
    private DiscoveryClient discoveryClient;

    @Resource
    private RestTemplate template;


    @GetMapping("/order/getOrder/{id}")
    public CommonResult<Payment> getOrder(@PathVariable Integer id){
        return template.getForObject(url+"/payment/get/" + id,CommonResult.class);

    }



    @GetMapping("/lb")
    public String testLb(){
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances("CLOUD-PROVIDER");
        ServiceInstance serviceInstance = loadBalancer.getServiceInstance(serviceInstances);
        if (ObjectUtils.isEmpty(serviceInstance))
            return "无此服务";
        URI uri = serviceInstance.getUri();
        System.out.println(uri + "/getPort");
        //ResponseEntity<String> entity = template.getForEntity(uri + "/getPort", String.class);
        return template.getForObject(uri + "/getPort",String.class);
        //return entity.getBody();
    }
}
