package com.dream.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Date:2021/6/28 20:57
 * @Author: zhouben
 */

//@Configuration
public class FeignConfig {
    //@Bean
    public Logger.Level level(){
        /**
         * NONE: 不显示
         * BASIC:仅记录请求方法、URL、响应状态码、执行时间
         * HEADERS: 除BASIC，还有请求和响应头的信息
         * FULL: 除HEADERS,还有请求和响应的正文及元数据
         */
        return Logger.Level.FULL;
//        return Logger.Level.HEADERS;
//        return Logger.Level.BASIC;
    }
}
