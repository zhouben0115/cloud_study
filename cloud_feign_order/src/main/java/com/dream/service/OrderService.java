package com.dream.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

//@Component
@FeignClient(value = "CLOUD-PROVIDER")
public interface OrderService {
    @GetMapping(value = "/getPort")
    String getServerPort();
}
