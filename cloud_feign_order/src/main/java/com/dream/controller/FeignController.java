package com.dream.controller;

import com.dream.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Date:2021/6/28 20:24
 * @Author: zhouben
 */

@RestController
public class FeignController {

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/feign/getPort")
    public String getServerPort(){
        return orderService.getServerPort();
    }
}
