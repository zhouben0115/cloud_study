package com.dream.service.impl;

import com.dream.dao.OrderMapper;
import com.dream.entity.Payment;
import com.dream.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @auther zzyy
 * @create 2020-02-18 10:40
 */
@Service
public class PaymentServiceImpl implements PaymentService
{
    @Autowired
    private OrderMapper paymentDao;

    public Payment getById(Long id) {
        return paymentDao.getById(id);
    }
}
