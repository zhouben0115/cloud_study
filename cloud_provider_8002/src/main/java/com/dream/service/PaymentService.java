package com.dream.service;


import com.dream.entity.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @auther zzyy
 * @create 2020-02-18 10:40
 */
public interface PaymentService
{
    Payment getById(@Param("id") Long id);
}
