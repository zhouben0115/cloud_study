package com.dream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Date:2021/6/27 11:40
 * @Author: zhouben
 */

@SpringBootApplication
@EnableDiscoveryClient
public class ZKApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZKApplication.class,args);
    }
}
