package com.dream.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Date:2021/7/10 11:28
 * @Author: zhouben
 */

@RestController
@RefreshScope
public class ConfigClientController {

    @Value("${spring.application.name}")
    private String info;



    @GetMapping("/info")
    public String info(){
        return info;
    }


}