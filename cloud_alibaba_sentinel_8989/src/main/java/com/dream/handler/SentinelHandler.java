package com.dream.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;

/**
 * @Date:2021/7/17 15:38
 * @Author: zhouben
 */

public class SentinelHandler {
    /**
     * ①方法要是static修饰的
     * ②参数和controller层的一致，必须携带BlockException异常类的参数
     * ③返回值要和controller层的一致
     * 看官网:https://spring-cloud-alibaba-group.github.io/github-pages/hoxton/en-us/index.html#_resttemplate_support
     */
    public static String handlerOne(String id, BlockException exception){
        return  "统一异常处理   =====>   " + id  + exception.getClass().getCanonicalName().toString();
    }
}
