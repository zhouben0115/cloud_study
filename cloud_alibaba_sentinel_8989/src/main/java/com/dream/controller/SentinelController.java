package com.dream.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.dream.entity.CommonResult;
import com.dream.entity.Payment;
import com.dream.handler.SentinelHandler;
import com.dream.service.PaymentService;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @Date:2021/7/13 19:53
 * @Author: zhouben
 */

@RestController
public class SentinelController {


    @GetMapping("/testA")
    public String testA() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("=======>");
        int i = 1 / 0;
        return "=======TestA========";
    }


    @GetMapping("/testB")
    @SentinelResource(value = "testB", blockHandler = "deal_testB")
    public String testB(@RequestParam(required = false) String p1,
                        @RequestParam(required = false) String p2
    ) {
        return "=======TestB========";
    }


    public String deal_testB(String p1, String p2, BlockException exception) {
        return "deal_deal_testB=======";
    }

    /**
     * 自定义限流处理
     *
     * @SentinelResource
     */
    @GetMapping("/testSentinelResource")
    @SentinelResource(value = "testSentinelResource", blockHandlerClass = SentinelHandler.class, blockHandler = "handlerOne")
    public String testSentinelResources(String id) {
        return "test=======>      @SentinelResource=======>" + id;
    }


    /**
     * 测试Ribbon + openFeign
     */


    @Autowired
    private RestTemplate template;

    private static final String URL = "http://service-provider-service";

    @GetMapping("/test/{id}")
    @SentinelResource(value = "test", fallback = "handlerFallback", blockHandler = "blockHandler")
    public CommonResult<Payment> test01(@PathVariable Long id) {

        if (id > 5) {
            throw new RuntimeException("error");
        }
        return template.getForObject(URL + "/paymentSQL/" + id, CommonResult.class);
    }


    //本例是fallback
    public CommonResult handlerFallback(@PathVariable Long id, Throwable e) {

        Payment payment = new Payment(id, "null");
        return new CommonResult<>(444, "兜底异常handlerFallback,exception内容  " + e.getMessage(), payment);
    }

    //本例是blockHandler
    public CommonResult blockHandler(@PathVariable Long id, BlockException blockException) {
        Payment payment = new Payment(id, "null");
        return new CommonResult<>(445, "blockHandler-sentinel限流,无此流水: blockException  " + blockException.getMessage(), payment);
    }





    @Resource
    private PaymentService paymentService;

    @GetMapping(value = "/consumer/paymentSQL/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id) {
        return paymentService.paymentSQL(id);
    }



}
