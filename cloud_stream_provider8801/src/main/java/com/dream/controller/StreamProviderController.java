package com.dream.controller;

import com.dream.service.MessageService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Date:2021/7/11 9:19
 * @Author: zhouben
 */

@RestController
public class StreamProviderController {

    @Resource
    private MessageService messageService;

    @RequestMapping("/sendMessage")
    public void sendMessage(){
        messageService.sendMessage();
    }
}
