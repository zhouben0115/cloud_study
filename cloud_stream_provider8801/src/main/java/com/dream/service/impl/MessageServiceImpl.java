package com.dream.service.impl;

import com.dream.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

/**
 * @Date:2021/7/11 10:05
 * @Author: zhouben
 */

@EnableBinding(Source.class)
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageChannel output;
    @Override
    public void sendMessage() {
        output.send(MessageBuilder.withPayload("我是生产者").build());
    }
}
