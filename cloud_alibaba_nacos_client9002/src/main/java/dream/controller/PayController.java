package dream.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Date:2021/7/11 18:12
 * @Author: zhouben
 */

@RestController
public class PayController {


    @Value("${server.port}")
    private String port;

//    @RequestMapping(value = "/getPort",method = RequestMethod.GET)
    @GetMapping("/getPort")
    public String getPort(){

        return "pay======>" + port;
    }

}
