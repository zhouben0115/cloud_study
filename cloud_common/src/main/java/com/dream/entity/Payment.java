package com.dream.entity;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.PascalNameFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable
{
    private Long id;
    private String serial;

    public static void main(String[] args) {
        String jsonArr = "[{\"name\":\"12\",\"age\":23},{\"name\":\"123\",\"age\":11}]";

        List<Map<String, String>> maps = JSONObject.parseObject(jsonArr, new TypeReference<List<Map<String, String>>>() {
        });


        String str = "{\"name\":\"12\",\"age\":23}";
        System.out.println(JSONObject.toJSONString(str, new PascalNameFilter()));
        System.out.println(maps);

    }
}



